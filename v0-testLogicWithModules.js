var express = require('express');
var bodyParser = require('body-parser');
//npm i -s body-parser

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

var possibleOutcomes = [
    "scissors beats paper",
    "paper beats rock",
    "rock beats scissors"
];

var objects = ["scissors", "paper", "rock"];

var games = []; //{game:number, userName:string, userTool:string, computerTool:string, winner:string}
var game = 0;

app.get('/games', (req, res) => {
    res.status(200).send(games);
});

app.get('/lastGame', (req, res) => {
    res.status(200).send(games[games.length-1]);
});

app.post('/game', (req, res) => {
    console.log("clientGameResults", req.body);
    let diceUser = req.body.result;
    let userName = req.body.name;
    let diceComputer = rollYourDiceComputer();

    toolUser = objects[diceUser];
    toolComputer = objects[diceComputer];
    console.log("user has ", toolUser);
    console.log("computer has ", toolComputer);

    winner = whoWins(diceUser, diceComputer);
    console.log("Who won?", winner);

    game++;
    games.push({
        game: game,
        name: userName,
        userTool: toolUser,
        computerTool: toolComputer,
        winner: winner
    });
    io.emit('gameOutcome', games[games.length - 1]);
    res.status(200).send(games[games.length-1]);  
});


function rollYourDiceComputer() {
    let result = Math.floor(Math.random() * 3);
    if(result>=3) result=2;
    return result;
}

function whoWins(user, computer) {
    let outcome = objects[user] + " beats " + objects[computer];
    console.log("CheckIf:", outcome);
    if (user == computer) {
        return "tie"
    }
    if (searchElementInArray(outcome, possibleOutcomes)) {
        return "user";
    } else {
        return "computer";
    }
}

function searchElementInArray(el, arr) {
    var found = false;
    for (var i = 0; i < arr.length && !found; i++) {
        if (arr[i] === el) {
            found = true;
            break;
        }
    }
    return found;
}

io.on('connection', (socket) => {
    console.log("someone has connected");
});

var server = http.listen(3000, () => {
    console.log('Server is listening on port', server.address().port);
});
