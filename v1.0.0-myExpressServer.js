var http = require('http');
var fs = require('fs');
var path = require('path');

function MyExpress(static) {

    var _path = "./";
    
    (static !== undefined) ? _path = static: _path = "./";

    function Server(getRoutes, PostPutRoutes) {
        return http.createServer((req,res)=>{

            console.log('Server is listening on port 3000...');

            //variables definition
            var body = '';
            filePath = req.url;
            (filePath == '/') ? filePath = "./index-v0.html": filePath = "." + filePath;
                        
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");            

            //EVENTS
            res.on('error', (err) => {console.error(err); res.statusCode = 400; res.end();});            
            req.on('error', (err) => {console.error(err); res.statusCode = 400; res.end();});
            req.on('data', (chunk) => body += chunk.toString());
            req.on('end', () =>{ PostPutRoutes(req,res,body); });             

            //NORMAL FLOW 
            getRoutes(req,res, ReturnFile);
        });
    }


    function ReturnFile(filePath, response, contentType) {
        fs.readFile(filePath, function (error, content) {
            if (error) {
                if (error.code == 'ENOENT') {
                    fs.readFile('./404.html', function (error, content) {
                        response.statusCode=200;
                        response.setHeader('Content-Type', contentType);                        
                        response.end();
                    });
                } else {
                    response.statusCode=500;
                    response.write('oops: ' + error.code + ' ..\n');                    
                    response.end();
                }
            } else {
                response.statusCode=200;
                response.setHeader('Content-Type', contentType);
                response.write(content, 'utf-8');                
                response.end();
            }            
        });
    }

    return {Server: Server};        
}

module.exports = MyExpress;
