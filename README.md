**Rock Paper Scissors - Camilo Chaves Solution **  
Game was on Google Play and Apple Store but had to be removed because it contains Disney characters.
But you can download the APK and install on your Android mobile 

**V1.5.0**  
1. Online Tournament Complete with Emperor and Vader Scenes  
2. Minor graphic improvements on Vader x User game mode

**NEXT VERSION 1.5.1**  
- Graphic improvements on game Vader x ObiWan

**Future developments**  
- Vader scenes with video insertion

**THIS GAME WAS PART OF A REQUESTED JOB EXERCISE**  
Acceptance Criteria:  
Can I play Player vs Computer? **YES**  
Can I play Computer vs Computer? **YES**  
Can I play a different game each time? **YES**  

Technical Constraints:  
Use Javascript ? **YES for the NODE.JS server, but uses typescript for the FRONTEND (typescript is javascript on steroids - check the elegance of the code in the mobile folder)**  
Libs / external modules should only be used for tests -> **test version made with modules was named V0. A Version for the server without modules was done in Javascript only and uploaded as V1, but check folder NODE_DB_GAME_API and you will see an complete NODE API communicating with MONGO CLOUD, to run set on a linux machine external variables usuario=camilo and senha=pwdcamilo**  

Is your solution visually creative and appealing?  **Not yet.. but working through it**  
Consider how your UX design is influenced by your target user.  Who are you building this for?  **I've build it for everyone. I've had so much fun doing it**  
We are keen to see how much you think is enough, and how much would go into a Minimum Viable Product. **enough for me is to deliver a minimum viable product fast (time is money) - I could always make it beautiful later once all logic is working fine, but for this case, I think it's good enough counting it was made in only 2 hard working days with 14hrs of coding in each**  
As a guide, elegant and simple wins over feature rich every time, though extra gold stars are given to people who get excited and do more because they are having fun. **noted**  

You can decide to use a server side back end, or make all the logic in the client.  **server side AND frontend Android app was made**  
How many browsers does your solution work with?  **all modern browsers**  
Please specify which is your target browser  **Chrome**  
Bonus points for cross-browser support **thanks**  
If it works on a smartphone or tablet, that would be even better!  **of course - Just download and install the APK that I've done**  

Run / build instructions are seen in a positive light, as it indicates you know how to work in that environment **for Versions above 0, without Modules, just download to your machine and run node V-X.X.X-nodeServer-WithoutModules.js, then open a browser and type http://localhost:3000 and a homepage should pop up. Be aware that the homepage game is extremely poor compared to the Mobile app that I've developed. For the mobile app install on your device the apk provided or download directly from Google Play Store or appleStore link above**  

**The mobile app comunicates with a Digital Ocean server configured specifically for this exercise. **  

**check advanced API's on NODE_DB_GAME_API, routes folder**  
  