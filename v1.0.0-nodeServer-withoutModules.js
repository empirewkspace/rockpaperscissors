 MyExpress = require("./v1.0.0-myExpressServer.js");

 //GAME ROCK PAPER OR SCISSORS
 //author: Camilo

var app = MyExpress();

global.games = []; //{game:number, userName:string, userTool:string, computerTool:string, winner:string}
global.game = 0;

var possibleOutcomes = [
    "scissors beats paper",
    "paper beats rock",
    "rock beats scissors"
];

var objects = {
    1: "scissors",
    2: "paper",
    3: "rock"
};

app.Server().on('connection', () => {
    console.log('someone has connected..');
});

function PostPutRoutes(req,res,body) {
    if (req.url === "/game" && body!==undefined) {                
        res.setHeader('Content-Type', 'application/json');
        body = JSON.parse(body);
        
        console.log("clientGameResults", body);
        let diceUser = body.result;
        let userName = body.name;
        let diceComputer = rollYourDiceComputer();

        toolUser=objects[diceUser];
        toolComputer= objects[diceComputer];
        console.log("computer has ", toolUser);
        console.log("computer has ", toolComputer);
    
        winner = whoWins(diceUser, diceComputer);
        console.log("Who won?", winner);

        global.game++;
        global.games.push({game:game, name:userName,userTool:toolUser, computerTool:toolComputer, winner:winner});

        res.write(JSON.stringify(global.games[global.games.length-1]));
        res.end();  
    }
}
function GetRoutes(req, res, sendFile) {    
    if (req.url === "/") {        
        sendFile('./v1.0.0-index.html',res,'text/html');        
    }

    if (req.url === "/games") {                
        res.setHeader('Content-Type', 'application/json');
        res.statusCode=200;                
        res.write(JSON.stringify(global.games));
        res.end();
    }

    if (req.url === "/lastGame") {                
        res.setHeader('Content-Type', 'application/json');
        res.statusCode=200;                
        let noGameYet='{"games":"0"}';
        res.write((global.games.length==0)? noGameYet: JSON.stringify(global.games[global.games.length-1]));
        res.end();
    }
}

function rollYourDiceComputer() {
    let result = Math.floor(Math.random() * 3);
    if(result>=3) result=2;
    return result;
}

function whoWins(user, computer) {
    let outcome = objects[user] + " beats " + objects[computer];
    console.log("CheckIf:", outcome);
    if (user == computer) {
        return "tie"
    }
    if (searchElementInArray(outcome,possibleOutcomes)) {
        return "user";
    } else {
        return "computer";
    }
}

function searchElementInArray(el,arr) {
    var found = false;
    for (var i = 0; i < arr.length && !found; i++) {
        if (arr[i] === el) {
            found = true;
            break;
        }
    }
    return found;
}

console.log('Server is listening on port 3001...');
app.Server(GetRoutes, PostPutRoutes).listen(3001);