import { Injectable, ModuleWithComponentFactories } from '@angular/core';

export interface Game { 
  game: number, userTool: string, 
  computerTool: string, outcome:string, 
  winner: string 
}


@Injectable()
export class GameLogicProvider {

  private games:Game[] = [];
  private game:number = 0;
  private possibleOutcomes = {
     "scissors paper":"scissors cuts paper", 
     "paper rock":"paper covers rock",
     "rock scissors":"rock crushes scissors",
     "spock rock":"spock vaporizes rock",
     "spock scissors":"spock smashes scissors",
     "paper spock":"paper disproves spock",
     "scissors lizard":"scissors decapitates lizard",
     "lizard paper":"lizard eats paper",
     "rock lizard":"rock crushes lizard",
     "lizard spock":"lizard poisons spock"
    };

  public objects = ["scissors", "paper", "rock" , "spock", "lizard"];

  constructor() {

  }

  private rollYourDice(n) {        
    let result = Math.floor(Math.random()*n);  
    if(result==this.objects.length) result--;
    return result;
  }

  private whoWins(user, computer): {winner:string, outcome:string} {    
    let outcome = this.objects[user] + " " + this.objects[computer];        
    if (user == computer) {
      return {winner:"tie", outcome:"tie"};
    }
    if (this.possibleOutcomes[outcome]) {
      return {winner:"user",outcome:this.possibleOutcomes[outcome]};
    } else {
      outcome = this.objects[computer] + " " + this.objects[user];        
      return {winner:"computer", outcome:this.possibleOutcomes[outcome]};
    }  
  }

  public Play(Nelements:number, userChoice?:number):Game{ //Nelements 3-4 --> 4 for Spock implementation
    let user = userChoice;
    let computer = this.rollYourDice(Nelements);      
    if(userChoice==undefined||userChoice==null) user=this.rollYourDice(Nelements); //if userchoice is undefined means computer vs computer      
    const {winner, outcome} = this.whoWins(user, computer);
    return {game:this.game, userTool: this.objects[user], computerTool:this.objects[computer], outcome:outcome,  winner:winner};
  }

  public Title(wins: number, losses: number) {
    let result = wins - losses;
    if (result < 0) return 'Minion';
    if (result >= 0 && result < 5) return 'Apprentice';
    if (result >= 5 && result < 10) return 'Sith';
    if (result >= 10 && result < 15) return 'Sith Marauder';
    if (result >= 15 && result < 20) return 'Sith Master';
    if (result >= 20 && result < 25) return 'Sith Lord';
    if (result >= 25) return 'Dark Lord';
  }

  
}


