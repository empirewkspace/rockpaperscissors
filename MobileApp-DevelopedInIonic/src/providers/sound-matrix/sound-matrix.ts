import { Injectable } from '@angular/core';
import { NativeAudio } from '../../../node_modules/@ionic-native/native-audio';

@Injectable()
export class SoundMatrixProvider {

  private soundLib = [
    { id: 'deathStar', uri: 'assets/mp3/deathStar.mp3' },
    { id: 'dontDoThisAnakin', uri: 'assets/mp3/dontDoThisAnakin.mp3' },
    { id: 'dontTouchMe', uri: 'assets/mp3/dontTouchMe.mp3' },
    { id: 'iAmLeaving', uri: 'assets/mp3/iAmLeaving.mp3' },
    { id: 'iCanStillFeelTheGoodInYou', uri: 'assets/mp3/iCanStillFeelTheGoodInYou.mp3' },
    { id: 'iToldYou', uri: 'assets/mp3/iToldYou.mp3' },
    { id: 'iWonAgain', uri: 'assets/mp3/iWonAgain.mp3' },
    { id: 'iWonHa', uri: 'assets/mp3/iWonHa.mp3' },
    { id: 'lastMistake', uri: 'assets/mp3/lastMistake.mp3' },
    { id: 'lightSaber', uri: 'assets/mp3/lightSaberTurnOn.mp3' },
    { id: 'looser', uri: 'assets/mp3/looser.mp3' },
    { id: 'myAbilitiesHaveImproved', uri: 'assets/mp3/myAbilitiesHaveImproved.mp3' },
    { id: 'ouch', uri: 'assets/mp3/ouch.mp3' },
    { id: 'padawan', uri: "assets/mp3/padawanChallenge.mp3" },
    { id: 'stopIt', uri: "assets/mp3/stopIt.mp3" },
    { id: 'stopPesteringMe', uri: "assets/mp3/stopPesteringMe.mp3" },
    { id: 'thatCantBePossible', uri: "assets/mp3/thatCantBePossible.mp3" },
    { id: 'thatDidntCount', uri: "assets/mp3/thatDidntCount.mp3" },
    { id: 'theForceIsStrongInYou', uri: "assets/mp3/theForceIsStrongInYou.mp3" },
    { id: 'waitWhatThatIsImpossible', uri: "assets/mp3/waitWhatThatIsImpossible.mp3" },
    { id: 'weakOldMan', uri: "assets/mp3/weakOldMan.mp3" },
    { id: 'weMeetAgain', uri: "assets/mp3/weMeetAgain.mp3" },
    { id: 'what', uri: "assets/mp3/what.mp3" },
    { id: 'whatDoYouWant', uri: "assets/mp3/whatDoYouWant.mp3" },
    { id: 'whoIsYourDaddyNow', uri: "assets/mp3/whoIsYourDaddyNow.mp3" },
    { id: 'youAreCheating', uri: "assets/mp3/youAreCheating.mp3" },
    { id: 'youIgnorantFool', uri: "assets/mp3/youIgnorantFool.mp3" },
    { id: 'youUnderestimateMyPower', uri: "assets/mp3/youUnderestimateMyPower.mp3" },
    { id: 'youWillPayForThatInsolence', uri: "assets/mp3/youWillPayForThatInsolence.mp3" },
    { id: 'clash1', uri: "assets/mp3/lightsaberclash1.mp3" },
    { id: 'clash2', uri: "assets/mp3/lightsaberclash2.mp3" },
    { id: 'clash3', uri: "assets/mp3/lightsaberclash3.mp3" },
    { id: 'clash4', uri: "assets/mp3/lightsaberclash4.mp3" },
    { id: 'hummm', uri: "assets/mp3/lightsaberhum.mp3" },
    { id: 'whatsWrongMacFlyChicken', uri: "assets/mp3/biffChicken.mp3" },
    { id: 'duelOfTheFates', uri: "assets/mp3/duelOfTheFates.mp3" },
    { id: 'emperorDispleased1', uri: "assets/mp3/emperorDispleased1.mp3" },
    { id: 'emperorDispleased2', uri: "assets/mp3/emperorDispleased2.mp3" },
    { id: 'emperorPleased1', uri: "assets/mp3/emperorPleased1.mp3" },
    { id: 'emperorPleased2', uri: "assets/mp3/emperorPleased2.mp3" },
    { id: 'emperorPleased3', uri: "assets/mp3/emperorPleased3.mp3" },
    { id: 'emperorPleased4', uri: "assets/mp3/emperorPleased4.mp3" },
    { id: 'vaderDisbelief1', uri: "assets/mp3/vaderDisbelief1.mp3" },
    { id: 'vaderDisbelief2', uri: "assets/mp3/vaderDisbelief2.mp3" },
    { id: 'vaderDisbelief3', uri: "assets/mp3/vaderDisbelief3.mp3" },
    { id: 'vaderDisbelief4', uri: "assets/mp3/vaderDisbelief4.mp3" },
    { id: 'vaderDisbelief5', uri: "assets/mp3/vaderDisbelief5.mp3" },
    { id: 'vaderDisbelief6', uri: "assets/mp3/vaderDisbelief6.mp3" },
    { id: 'vaderDisbelief7', uri: "assets/mp3/vaderDisbelief7.mp3" },
    { id: 'vaderDispleased1', uri: "assets/mp3/vaderDispleased1.mp3" },
    { id: 'vaderDispleased2', uri: "assets/mp3/vaderDispleased2.mp3" },
    { id: 'vaderDispleased3', uri: "assets/mp3/vaderDispleased3.mp3" },
    { id: 'vaderDispleased4', uri: "assets/mp3/vaderDispleased4.mp3" },
    { id: 'vaderDispleased5', uri: "assets/mp3/vaderDispleased5.mp3" },
    { id: 'vaderDispleased6', uri: "assets/mp3/vaderDispleased6.mp3" },
    { id: 'vaderDispleased7', uri: "assets/mp3/vaderDispleased7.mp3" },
    { id: 'vaderDispleased8', uri: "assets/mp3/vaderDispleased8.mp3" },
    { id: 'vaderDispleased9', uri: "assets/mp3/vaderDispleased9.mp3" },
    { id: 'vaderFails1', uri: "assets/mp3/vaderFails1.mp3" },
    { id: 'vaderFails2', uri: "assets/mp3/vaderFails2.mp3" },
    { id: 'vaderFails3', uri: "assets/mp3/vaderFails3.mp3" },
    { id: 'vaderFails4', uri: "assets/mp3/vaderFails4.mp3" },
    { id: 'vaderFails5', uri: "assets/mp3/vaderFails5.mp3" },
    { id: 'vaderFails6', uri: "assets/mp3/vaderFails6.mp3" },
    { id: 'vaderHappy1', uri: "assets/mp3/vaderHappy1.mp3" },
    { id: 'vaderHappy2', uri: "assets/mp3/vaderHappy2.mp3" },
    { id: 'vaderHappy3', uri: "assets/mp3/vaderHappy3.mp3" },
    { id: 'vaderHappy4', uri: "assets/mp3/vaderHappy4.mp3" },
    { id: 'vaderHappy5', uri: "assets/mp3/vaderHappy5.mp3" },
    { id: 'vaderHappy6', uri: "assets/mp3/vaderHappy6.mp3" },
    { id: 'vaderHappy7', uri: "assets/mp3/vaderHappy7.mp3" },
    { id: 'vaderHappy8', uri: "assets/mp3/vaderHappy8.mp3" },
    { id: 'vaderVeryDispleased1', uri: "assets/mp3/vaderVeryDispleased1.mp3" },
    { id: 'vaderVeryDispleased2', uri: "assets/mp3/vaderVeryDispleased2.mp3" },
    { id: 'vaderVeryDispleased3', uri: "assets/mp3/vaderVeryDispleased3.mp3" },
    { id: 'vaderVeryDispleased4', uri: "assets/mp3/vaderVeryDispleased4.mp3" },
    { id: 'vaderVeryDispleased5', uri: "assets/mp3/vaderVeryDispleased5.mp3" },
    { id: 'vaderVeryHappy1', uri: "assets/mp3/vaderVeryHappy1.mp3" },
    { id: 'vaderVeryHappy2', uri: "assets/mp3/vaderVeryHappy2.mp3" },
    { id: 'vaderVeryHappy3', uri: "assets/mp3/vaderVeryHappy1.mp3" },
    { id: 'vaderVeryHappy4', uri: "assets/mp3/vaderVeryHappy2.mp3" },
    { id: 'themeVaderLost', uri: "assets/mp3/themeVaderLost.mp3" },
    { id: 'themeVaderWon', uri: "assets/mp3/themeVaderWon.mp3" },
    { id: 'themeOnlineTournament', uri: "assets/mp3/themeOnlineTournament.mp3" },
    { id: 'themeLobbyAndGameRoom', uri: "assets/mp3/themeLobbyAndGameRoom.mp3" },
    { id: 'themeFightArena', uri: "assets/mp3/themeFightArena.mp3" },
    { id: 'themeEmperorLost', uri: "assets/mp3/themeEmperorLost.mp3" },
    { id: 'themeVaderIntro', uri: "assets/mp3/themeVaderIntro.mp3" },
    { id: 'themeVaderLeaves', uri: "assets/mp3/themeVaderLeaves.mp3" },
    { id: 'themeEmperorTalking', uri: "assets/mp3/themeEmperorTalking.mp3" },
    { id: 'themeVaderGuitarStarWars', uri: "assets/mp3/themeVaderGuitarStarWars.mp3" },
    { id: 'themeVaderGuitarImperialMarch', uri: "assets/mp3/themeVaderGuitarImperialMarch.mp3" },
    { id: 'emperorWelcomeA', uri: "assets/mp3/emperorWelcomeA.mp3" },
    { id: 'emperorWelcomeB', uri: "assets/mp3/emperorWelcomeB.mp3" },
    { id: 'emperorWelcomeC', uri: "assets/mp3/emperorWelcomeC.mp3" },
    { id: 'emperorWelcomeD', uri: "assets/mp3/emperorWelcomeD.mp3" },
    { id: 'emperorWelcomeE', uri: "assets/mp3/emperorWelcomeE.mp3" },
    { id: 'emperorWelcomeF', uri: "assets/mp3/emperorWelcomeF.mp3" },
    { id: 'emperorWelcomeG', uri: "assets/mp3/emperorWelcomeG.mp3" },
    { id: 'emperorWelcomeH', uri: "assets/mp3/emperorWelcomeH.mp3" },
    { id: 'emperorWelcomeI', uri: "assets/mp3/emperorWelcomeI.mp3" },
    { id: 'emperorWelcomeJ', uri: "assets/mp3/emperorWelcomeJ.mp3" },
    { id: 'emperorWelcomeK', uri: "assets/mp3/emperorWelcomeK.mp3" },
    { id: 'emperorWelcomeL', uri: "assets/mp3/emperorWelcomeL.mp3" },
    { id: 'emperorWelcomeM', uri: "assets/mp3/emperorWelcomeM.mp3" },
    { id: 'emperorWelcomeN', uri: "assets/mp3/emperorWelcomeN.mp3" },
    { id: 'emperorWelcomeO', uri: "assets/mp3/emperorWelcomeO.mp3" },
    { id: 'emperorWelcomeP', uri: "assets/mp3/emperorWelcomeP.mp3" },
    { id: 'emperorWelcomeQ', uri: "assets/mp3/emperorWelcomeQ.mp3" },
    { id: 'emperorWelcomeR', uri: "assets/mp3/emperorWelcomeR.mp3" },
    { id: 'emperorWelcomeS', uri: "assets/mp3/emperorWelcomeS.mp3" },
    { id: 'emperorWelcomeT', uri: "assets/mp3/emperorWelcomeT.mp3" },
    { id: 'emperorWelcomeU', uri: "assets/mp3/emperorWelcomeU.mp3" },
    { id: 'emperorWelcomeV', uri: "assets/mp3/emperorWelcomeV.mp3" },
    { id: 'emperorWelcomeW', uri: "assets/mp3/emperorWelcomeW.mp3" },
    { id: 'emperorWelcomeX', uri: "assets/mp3/emperorWelcomeX.mp3" },
    { id: 'emperorWelcomeY', uri: "assets/mp3/emperorWelcomeY.mp3" },
    { id: 'emperorWelcomeZ', uri: "assets/mp3/emperorWelcomeZ.mp3" }   
  ];

  constructor(public nativeAudio: NativeAudio) {

  }

  async Play(...sound: string[]): Promise<boolean> {
    let i = 0;
    try {
      while (i < sound.length) {
        await this.nativeAudio.preloadComplex(
          sound[i], this.soundLib.find(p => p.id === sound[i]).uri,1,1,0);
        await this.PlayNative(sound[i]);
        await this.nativeAudio.unload(sound[i]);
        i++;
      }
    } catch (e) {
      console.log("something went wrong with the audio:", e); return Promise.resolve(true);
    }
    return Promise.resolve(true);
  }

  async Stop(sound: string):Promise<boolean>{
    try{
    await this.nativeAudio.stop(sound);
    await this.nativeAudio.unload(sound);
    return Promise.resolve(true);
    }catch (e){console.log(e); return Promise.resolve(true);}    
  }

  private PlayNative(sound: string): Promise<boolean> {
    return new Promise((resolve,reject) => {
      this.nativeAudio.play(sound, () => {        
        resolve(true);
      }).catch(e=>{console.log(e); reject(false);});
    });
  }

  async PlayMusicOnLoop(sound: string,volume?: number): Promise<boolean>{
    if(volume==undefined) volume=1;
    try{
    await this.nativeAudio.preloadComplex(
      sound, this.soundLib.find(p => p.id === sound).uri,volume,1,0);
    this.nativeAudio.loop(sound);
    return Promise.resolve(true);
    } catch(e) {console.log(e); Promise.resolve(true);}

  }

}
