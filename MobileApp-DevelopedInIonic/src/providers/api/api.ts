import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { timeout, retry, tap } from '../../../node_modules/rxjs/operators';
import { DataProvider, User } from '../data/data';

export interface game {
  game: number,
  name: string,
  userTool: string,
  computerTool: string,
  winner: string
}

export interface GameOutcome {
  user1Tool: string,
  user2Tool: string,
  outcome: string,
  winner: string
}

export interface GameRoom {
  _id: string;
  room: number,
  game: number,
  roomOwner: string,
  challenger: string,
  date: string,
  isPrivate: boolean,
  whoLockedRoom: string,
  password: string,
  roomOwnerChoice: number,
  challengerChoice: number,
  nElements: number,
  results: Array<GameOutcome>
}

@Injectable()
export class ApiProvider {

  constructor(public http: HttpClient, public data: DataProvider) {

  }

  callServer(data: { result: number, name: string }): Promise<void | game> {
    const server = 'http://159.65.238.114:3000/game';
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    });
    
    return this.http.post(server, data, { headers })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API POST', results))
      )
      .toPromise()
      .then((val: game) => val)
      .catch((e) => {console.log(e);alert('Server offline!');});
  }

  getLastGame(): Promise<void | game> {
    const server = 'http://159.65.238.114:3000/lastGame';
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    });

    return this.http.get(server, { headers })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API GET', results))
      )
      .toPromise()
      .then((val: game) => val)
      .catch((e) => {console.log(e);alert('Server offline!');});
  }

  authenticateUser(email: string, password: string) {//WILL RETURN A TOKEN
    let server = this.data.server + ":3001/api/auth";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    });
    let params = { email: email, password: password }
    return this.http.post(server, params, { headers: headers, responseType: 'text' })
      .pipe(
        timeout(5000),
        retry(1)
      )
      .toPromise();
  }

  logUserInTheLobby(): Promise<User> {//Just need the token for that because
    //the token has a payload which carries the user mongo _id
    let server = this.data.server + ":3001/api/users/login";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',      
      'x-auth-token': this.data.token
    });

    return this.http.post(server, {}, { headers: headers })
      .pipe(
        timeout(5000),
        retry(1),
        tap(res => console.log('Response', res))
      )
      .toPromise()
      .then((user: User) => this.data.loggedUser = user);
  }

  logUserOutTheLobby(): Promise<User> {//Just need the token for that because
    //the token has a payload which carries the user mongo _id
    let server = this.data.server + ":3001/api/users/logout";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',      
      'x-auth-token': this.data.token
    });

    return this.http.post(server, {}, { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1)
      )
      .toPromise()
      .then((user: User) => user);
  }

  usersOnline(): Promise<any> {
    let server = this.data.server + ":3001/api/game/playersOnLine/"
      + this.data.pageNumber + '/' + this.data.numberOFElementsReturned;
      let headers = new HttpHeaders({      
        'Content-Type': 'application/json; charset=utf-8',
        'Access-Control-Allow-Origin': '*'
      });

    return this.http.get(server, { headers: headers })
      .pipe(
        timeout(5000),
        retry(1)
      )
      .toPromise();
  }

  createUser(name: string, email: string, password: string): Promise<User | void> {
    //Just need the token for that because
    //the token has a payload which carries the user mongo _id
    let server = this.data.server + ":3001/api/users";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*'      
    });

    let params = { email: email, name: name, password: password };

    return this.http.post(server, params, { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API createUser', results))
      )
      .toPromise()
      .then((user: User) => this.data.loggedUser = user);

  }

  availableGameRooms(): Promise<any> {
    let server = this.data.server + ":3001/api/game/availableGameRooms";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    });

    return this.http.get(server, { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API availableGameRooms', results))
      )
      .toPromise();
  }

  getUser(id: string): Promise<any> {
    let server = this.data.server + ":3001/api/users/readUserStats";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    });

    return this.http.post(server, { _id: id }, { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1),
        /* tap(results => console.log('API getUser', results)) */
      )
      .toPromise();

  }

  getMyUser(): Promise<any> {
    let server = this.data.server + ":3001/api/users/readUserStats";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'x-auth-token': this.data.token
    });

    return this.http.post(server, {}, { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API getUser', results))
      )
      .toPromise();

  }


  CreateGameRoom(nElements: number, isPrivate: boolean, password?: string)
    : Promise<GameRoom> {
    let server = this.data.server + ":3001/api/game/createGameRoom";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'x-auth-token': this.data.token
    });

    let params = null;
    if (password !== undefined) {
      params = { nElements: nElements, isPrivate: isPrivate, password: password };
    } else {
      params = { nElements: nElements, isPrivate: isPrivate };
    }

    return this.http.post(server, params, { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API CreateGameRoom', results))
      )
      .toPromise()
      .then((gameRoom: GameRoom) => gameRoom);
  }

/*   DeleteGameRoom(): Promise<any> {
    let server = this.data.server + ":3001/api/game/deleteGameRoom";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'x-auth-token': this.data.token
    });
    return this.http.delete(server, { headers: headers, responseType:"text" })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API DeleteGameRoom', results))
      )
      .toPromise();
  } */

  DeleteGameRoom(): Promise<any> {
    let server = this.data.server + ":3001/api/game/deleteGameRoomWithPost";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'x-auth-token': this.data.token
    });
    return this.http.post(server, {}, { headers: headers, responseType:"text" })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API DeleteGameRoom', results))
      )
      .toPromise();
  }

  GetGameRoom(id: string): Promise<GameRoom> {
    let server = this.data.server + ":3001/api/game/getGameRoom";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    });

    return this.http.post(server, { _id: id }, { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1),
        /* tap(results => console.log('API GetGameRoom', results)) */
      )
      .toPromise()
      .then((gameRoom: GameRoom) => gameRoom);
  }

  GetMyGameRoom(): Promise<GameRoom> {
    let server = this.data.server + ":3001/api/game/getGameRoom";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'x-auth-token': this.data.token
    });

    return this.http.post(server, {}, { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API GetGameRoom', results))
      )
      .toPromise()
      .then((gameRoom: GameRoom) => gameRoom);
  }

  ChallengerLeaveRoom(roomId: string): Promise<any> {
    let server = this.data.server + ":3001/api/game/challengerLeaveRoom";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'x-auth-token': this.data.token
    });

    return this.http.post(server, { _id: roomId }, { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API ChallengerLeaveRoom', results))
      )
      .toPromise();
  }

  ChallengerEnterRoom(roomId: string): Promise<any> {
    let server = this.data.server + ":3001/api/game/challengerEnterRoom";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'x-auth-token': this.data.token
    });

    return this.http.post(server, { _id: roomId }, { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API ChallengerEnterRoom', results))
      )
      .toPromise();
  }

  WriteUserChoiceOnGameRoom(roomId: string, userChoice: number): Promise<any> {
    let server = this.data.server + ":3001/api/game/writeUserChoiceOnGameRoom";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'x-auth-token': this.data.token
    });
    return this.http.post(server, { _id: roomId, userChoice: userChoice },
      { headers: headers })
      .pipe(
        timeout(5000),
        retry(1),
        /* tap(results => console.log('API WriteUserChoiceOnGameRoom', results)) */
      )
      .toPromise();
  }

  CalculateResults(roomId: string, roomOwnerChoice:number,
  challengerChoice?:number): Promise<any> {
    let server = this.data.server + ":3001/api/game/calculateResults";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'x-auth-token': this.data.token
    });
    let params = {_id: roomId, roomOwnerChoice: roomOwnerChoice,
    challengerChoice: challengerChoice}    
    return this.http.post(server, params, { headers: headers })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API calculateResults', results))
      )
      .toPromise();
  }

  ClearGameRoomResults(roomId: string): Promise<GameRoom> {
    let server = this.data.server + ":3001/api/game/clearGameRoomResults";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    });

    return this.http.post(server, { _id: roomId },
      { headers: headers, responseType: 'json' })
      .pipe(
        timeout(5000),
        retry(1),
        tap(results => console.log('API ClearGameRoomResults', results))
      )
      .toPromise()
      .then((gameRoom: GameRoom) => gameRoom);
  }
  
  LockGameRoom(roomId: string):Promise<any>{
    let server = this.data.server + ":3001/api/game/lockGameRoom";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',      
      'x-auth-token': this.data.token
    });
    return this.http.post(server, { _id: roomId},
      { headers: headers, responseType:'text'})
      .pipe(
        timeout(5000),
        retry(1)        
      )    
      .toPromise();
  }

  UnLockGameRoom(roomId: string):Promise<any>{
    let server = this.data.server + ":3001/api/game/unlockGameRoom";
    let headers = new HttpHeaders({      
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'x-auth-token': this.data.token
    });
    return this.http.post(server, { _id: roomId},
      { headers: headers, responseType:'text'})
      .pipe(
        timeout(5000),
        retry(1)        
      )    
      .toPromise();
  }

 
}
