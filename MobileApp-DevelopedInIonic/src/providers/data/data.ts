import { Injectable } from '@angular/core';

export class User {
  _id: string;
  online: boolean = false;
  wins?: number; losses?: number;
  name: string; email: string;  password: string;
}

export class LoginData {
  name: string; email: string;  password: string;
}

@Injectable()
export class DataProvider {

  public token: string;
  public server = "http://159.65.238.114"
  public loggedUser: User;
  public pageNumber: number = 1;
  public numberOFElementsReturned: number = 10;

  constructor() {
    
  }

}
