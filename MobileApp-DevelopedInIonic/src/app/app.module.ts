import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DeviceProvider } from '../providers/device/device';
import { ApiProvider } from '../providers/api/api';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { GameLogicProvider } from '../providers/game-logic/game-logic';
import { NativeAudio } from '@ionic-native/native-audio';
import { SoundMatrixProvider } from '../providers/sound-matrix/sound-matrix';
import { Camera } from '../../node_modules/@ionic-native/camera';
import { DataProvider } from '../providers/data/data';
import { SceneProvider } from '../providers/scene/scene';

@NgModule({
  declarations: [
    MyApp    
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DeviceProvider, ApiProvider, GameLogicProvider, NativeAudio,
    SoundMatrixProvider, Camera,
    DataProvider,
    SceneProvider
  ]
})
export class AppModule {}
