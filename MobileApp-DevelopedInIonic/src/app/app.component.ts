import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { SceneProvider } from '../providers/scene/scene';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = "OnlineTournamentPage";

  pages: Array<{title: string, component: any}>;

  private sceneIndex:number=0;

  constructor(public platform: Platform, 
    public statusBar: StatusBar, public splashScreen: SplashScreen,
    public modalCtrl:ModalController, 
    public scene: SceneProvider
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {    
    this.nav.setRoot(page);
  }

  


}
