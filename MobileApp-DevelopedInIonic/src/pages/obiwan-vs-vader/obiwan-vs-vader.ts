import { Component, ModuleWithComponentFactories } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { SoundMatrixProvider } from '../../providers/sound-matrix/sound-matrix';
import { GameLogicProvider, Game } from '../../providers/game-logic/game-logic';

@IonicPage()
@Component({
  selector: 'page-obiwan-vs-vader',
  templateUrl: 'obiwan-vs-vader.html',
})
export class ObiwanVsVaderPage {

  private fight = "assets/imgs/fightStart.png"
  private fightScenes = ['assets/imgs/fight1.jpg', 'assets/imgs/fight2.jpg', 
  'assets/imgs/fight4.jpg', 'assets/imgs/fight6.jpg'];
  private obiwanHits = ['assets/imgs/fight5.jpg'];
  private vaderHits = ['assets/imgs/fight3.jpg'];
  private lightSaberSounds = ['clash1', 'clash2', 'clash3', 'clash4'];
  private lightSaberTie = ['hummm'];
  private gameResult: Game;
  private scoreObi = 0;
  private scoreVader = 0;
  private interval: any;

  constructor(
    public sound: SoundMatrixProvider,
    private gameLogic: GameLogicProvider
  ) {
  }

  ionViewDidEnter() {    
    this.sound.Play('weMeetAgain', 'dontDoThisAnakin','myAbilitiesHaveImproved',
       'iCanStillFeelTheGoodInYou', 'youUnderestimateMyPower', 'lightSaber')
      .then(() => this.FightAction());     
  }

  ionViewWillLeave() {
    this.sound.nativeAudio.stop('duelOfTheFates').catch(e=>console.log(e)); 
    this.sound.nativeAudio.stop('weMeetAgain').catch(e=>console.log(e)); 
    this.sound.nativeAudio.stop('dontDoThisAnakin').catch(e=>console.log(e)); 
    this.sound.nativeAudio.stop('myAbilitiesHaveImproved').catch(e=>console.log(e)); 
    this.sound.nativeAudio.stop('iCanStillFeelTheGoodInYou').catch(e=>console.log(e)); 
    this.sound.nativeAudio.stop('youUnderestimateMyPower').catch(e=>console.log(e)); 
    this.sound.nativeAudio.stop('lightSaber').catch(e=>console.log(e)); 

    clearInterval(this.interval);
  }

  FightAction() {
    this.sound.Play('duelOfTheFates').catch(e=>console.log(e)); ; 
    this.interval = setInterval(() => {
      this.gameResult = this.gameLogic.Play(3);
      if (this.gameResult.winner == 'user') {//aka obiWan
        this.scoreObi++;        
        let random = this.getRandomIntInclusive(0, this.lightSaberSounds.length - 1);
        this.fight = this.obiwanHits[0];
        this.sound.Play(this.lightSaberSounds[random]).catch(e=>console.log(e));
      }
      if (this.gameResult.winner == 'computer') { //aka vader
        this.scoreVader++;        
        let random = this.getRandomIntInclusive(0, this.lightSaberSounds.length - 1);
        this.fight = this.vaderHits[0];
        this.sound.Play(this.lightSaberSounds[random]).catch(e=>console.log(e));
      } else { //then it's a tie
        let random = this.getRandomIntInclusive(0, this.fightScenes.length - 1);
        this.fight=this.fightScenes[random];
        this.sound.Play('hummm').catch(e=>console.log(e));
      }
    },4500);

  }

  getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
