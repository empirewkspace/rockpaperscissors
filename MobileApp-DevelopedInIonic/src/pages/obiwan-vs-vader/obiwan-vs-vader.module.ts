import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ObiwanVsVaderPage } from './obiwan-vs-vader';

@NgModule({
  declarations: [
    ObiwanVsVaderPage,
  ],
  imports: [
    IonicPageModule.forChild(ObiwanVsVaderPage),
  ],
})
export class ObiwanVsVaderPageModule {}
