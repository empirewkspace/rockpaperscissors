import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { User, DataProvider } from '../../providers/data/data';
import { ApiProvider, GameRoom } from '../../providers/api/api';
import { GameLogicProvider } from '../../providers/game-logic/game-logic';
import { SoundMatrixProvider } from '../../providers/sound-matrix/sound-matrix';
import { Movie, SceneProvider } from '../../providers/scene/scene';

@IonicPage()
@Component({
  selector: 'page-lobby',
  templateUrl: 'lobby.html',
})
export class LobbyPage {

  public usersOnline: User[] = [];
  public gameRooms: GameRoom[] = [];
  public loggedUser: User;
  private choice = "lobby";
  private user: Promise<User>;
  private loadedUsers: boolean = false;
  private loadedGameRooms: boolean = false;
  private updateInterval: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public data: DataProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public gameLogic: GameLogicProvider,
    public soundCtrl: SoundMatrixProvider,
    public modalCtrl: ModalController,
    public sceneProvider: SceneProvider
  ) {
  }

  async DoRefresh() {

    try {
      this.usersOnline = await this.api.usersOnline();
      this.gameRooms = await this.api.availableGameRooms();
      this.loadedUsers = true;
      this.loadedGameRooms = true;
    } catch (e) { alert(e.error) }
  }

  ionViewDidLoad() {
    this.DoRefresh();
  }

  async Movie(i: number): Promise<boolean> {
    let movie = this.sceneProvider.movieMatrix.find(p => p.movie == i);
    return await this.PlayMovie(movie);
  }

  PlayMovie(movie: Movie): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let mymodal = this.modalCtrl.create('VaderReactionsPage', { 'Movie': movie });
      mymodal.present();
      mymodal.onDidDismiss(() => resolve(true));
    });
  }

  async ionViewWillEnter() {
    this.loggedUser = this.data.loggedUser;
  }

  async ionViewDidEnter() {
    this.soundCtrl.PlayMusicOnLoop('themeLobbyAndGameRoom');

    this.updateInterval = setInterval(() => {
      this.DoRefresh();
    }, 3000);

    let roomOwnerIsChickenID = this.navParams.get('roomOwnerIsAChicken');
    let challengerIsChickenID = this.navParams.get('challengerIsAChicken');
    if (roomOwnerIsChickenID) {
      //are you the roomOwner ?
      if (roomOwnerIsChickenID == this.data.loggedUser._id) {
        await this.Movie(5);
      } else {
        await this.Movie(4);
      }
    }
    if (challengerIsChickenID) {
      if (challengerIsChickenID == this.data.loggedUser._id) {
        await this.Movie(5);
      } else {
        await this.Movie(4);
      }
    }

  }

  ionViewWillLeave() {
    this.soundCtrl.Stop('themeLobbyAndGameRoom');
    if (this.updateInterval) {
      clearInterval(this.updateInterval);
    }
  }

  Logout() {
    clearInterval(this.updateInterval);
    this.api.logUserOutTheLobby().then(user => {
      if (this.gameRooms.find(p => p.roomOwner == this.loggedUser._id)) {
        this.api.DeleteGameRoom().catch(e => alert(e.error));
      }
      this.data.token = '';
      this.navCtrl.setRoot('OnlineTournamentPage');
    }
    );
  }

  DeleteGameRoom() {
    this.api.DeleteGameRoom().catch(e => alert(e.error));
  }

  Title(wins: number, losses: number) {
    return this.gameLogic.Title(wins, losses);
  }

  segmentChanged(val) {

  }

  getUserName(gr: GameRoom) {
    return this.usersOnline.find(p => p._id == gr.roomOwner).name;
  }

  getUserTitle(gr: GameRoom) {
    let wins = this.usersOnline.find(p => p._id == gr.roomOwner).wins;
    let losses = this.usersOnline.find(p => p._id == gr.roomOwner).losses;
    return this.Title(wins, losses);
  }

  goToFightArena(gr: GameRoom) {
    //first check if room is private
    let passwordCorrect = false;
    if (gr.isPrivate) {
      let confirm = this.alertCtrl.create({
        title: 'Private Room', message: 'Enter Password:',
        enableBackdropDismiss: false, cssClass: 'alertCSS',
        inputs: [{ name: 'password', placeholder: 'Password' }],
        buttons: [
          {
            text: 'ok', cssClass: 'myOkButtonCSS', handler: (data) => {
              if (data.password == gr.password) {
                passwordCorrect = true;
              }
            }
          },
          {
            text: 'cancel', role: 'myNoButtonCSS', handler: () => {
              passwordCorrect = false;
            }
          }
        ]
      });

      confirm.present();
      confirm.onDidDismiss(data => {
        if (passwordCorrect) {
          this.api.ChallengerEnterRoom(gr._id).then(
            () => this.navCtrl.setRoot('BattleArenaPage', { 'gameRoom': gr })
          ).catch(e => alert(e.error));
        } else {
          alert('invalid password');
        }
      });

    } else {
      this.api.ChallengerEnterRoom(gr._id).then(
        () => this.navCtrl.setRoot('BattleArenaPage', { 'gameRoom': gr })
      ).catch(e => alert(e.error));
    }
  }

  CreateGameRoom() {
    this.RoomCreationSequence();
  }

  RoomCreationSequence() {
    let privateRoom = false;
    let privatePassword = '';
    let confirm = this.alertCtrl.create({
      title: 'GameRoom Creation', message: 'Is it a Private Room?',
      enableBackdropDismiss: false, cssClass: 'alertCSS',
      buttons: [
        {
          text: 'No, my master!', cssClass: 'myNoButtonCSS', handler: () => {
            this.api.CreateGameRoom(5, privateRoom).then(
              (gameRoomCreated) => {
                this.loadedGameRooms = false;
                let alert = this.alertCtrl.create({
                  title: 'Entering battle arena'
                });
                alert.present();
                setTimeout(() => {
                  alert.dismiss();
                  clearInterval(this.updateInterval);
                  this.navCtrl.setRoot('BattleArenaPage', { 'gameRoom': gameRoomCreated });
                }, 3000);
              }
            ).catch(e => { console.log(e); alert(e.error); });
          }
        },
        {
          text: 'Yes, my master!', cssClass: 'myOkButtonCSS', handler: () => {
            privateRoom = true;
          }
        }
      ]
    });
    confirm.present();
    confirm.onDidDismiss(() => {
      if (privateRoom) {
        let pwdAlert = this.alertCtrl.create({
          message: 'Room Private Password:',
          enableBackdropDismiss: false, cssClass: 'alertCSS2',
          inputs: [{ name: 'pwd', placeholder: 'Room Password' }],
          buttons: [{
            text: 'Ok', cssClass: 'myOkButtonCss', handler: (data) => {
              privatePassword = data.pwd;
            }
          }]
        });
        pwdAlert.present();
        pwdAlert.onDidDismiss(() => {
          if (privatePassword !== '') {
            this.api.CreateGameRoom(5, privateRoom, privatePassword).then(
              (gameRoomCreated) => {
                this.loadedGameRooms = false;
                let alert = this.alertCtrl.create({
                  title: 'Entering battle arena'
                });
                alert.present();
                setTimeout(() => {
                  alert.dismiss();
                  clearInterval(this.updateInterval);
                  this.navCtrl.setRoot('BattleArenaPage', { 'gameRoom': gameRoomCreated });
                }, 3000);
              }
            ).catch(e => alert(e.error));;
          }
        });
      }
    }
    );
  }

}
