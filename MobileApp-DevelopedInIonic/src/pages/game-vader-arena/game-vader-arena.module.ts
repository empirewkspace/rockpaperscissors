import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GameVaderArenaPage } from './game-vader-arena';

@NgModule({
  declarations: [
    GameVaderArenaPage,
  ],
  imports: [
    IonicPageModule.forChild(GameVaderArenaPage),
  ],
})
export class GameVaderArenaPageModule {}
