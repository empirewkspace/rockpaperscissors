import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ModalController } from 'ionic-angular';
import { GameLogicProvider, Game } from '../../providers/game-logic/game-logic';
import { SoundMatrixProvider } from '../../providers/sound-matrix/sound-matrix';
import { DeviceProvider } from '../../providers/device/device';

@IonicPage()
@Component({
  selector: 'page-game-vader-arena',
  templateUrl: 'game-vader-arena.html',
})
export class GameVaderArenaPage {

  
  private gameResult: Game;
  private scoreVader = 0;
  private scoreUser = 0;
  private userName: string;
  private nElements: number = 3;
  private shutUpVader: boolean = false;
  private userImg : string ="assets/imgs/userPlaceholder.png";
  private objs=[];
  private disablePlay = false;

  private vaderPissedLevel = 0;
  private vaderWonLastRound=false;  
  private userWonLastRound=false;
  private vaderReactions = ["dontTouchMe", "what", "whatDoYouWant", "stopIt", "stopPesteringMe", "youWillPayForThatInsolence","youIgnorantFool","deathStar","iAmLeaving"];
  private whenVaderWins=["iToldYou","looser","iWonHa"];
  private whenVaderWinsInSequence=["whoIsYourDaddyNow","iWonAgain"]
  private whenVaderLooses=["ouch","youAreCheating","waitWhatThatIsImpossible","thatDidntCount","stopIt"];
  private whenVaderLoosesInSequence=["thatCantBePossible","youWillPayForThatInsolence","theForceIsStrongInYou"];

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public gameAPI: GameLogicProvider,
    public sound: SoundMatrixProvider,
    public device: DeviceProvider,    
    public modalCtrl: ModalController
  ) {    

  }

  ionViewWillEnter() {
    this.vaderPissedLevel=0;
    this.userName = this.navParams.get('userName');
    this.nElements = this.navParams.get('nElements');    
    let i=0;
    while (i<this.nElements) {this.objs.push(this.gameAPI.objects[i]); i++;}
  }

  Play() {        
    let myModal = this.modalCtrl.create('ChoiceUserPage', { 'options': this.objs });
    myModal.present();
    myModal.onDidDismiss((userChoice)=>{      
      this.gameResult = this.gameAPI.Play(this.nElements, userChoice);
      if(this.gameResult.winner=='user') this.scoreUser++;
      if(this.gameResult.winner=='computer') this.scoreVader++;
      if(!this.shutUpVader){
        this.disablePlay=true;
        this.VaderAnnoys();
        this.disablePlay=false;
      }    
    });        
  }

  VaderTouched() {
    this.disablePlay=true;
    this.sound.Play(this.vaderReactions[this.vaderPissedLevel])
    .catch(e=>console.log(e));  
    this.disablePlay=false;  
    if(this.vaderPissedLevel==8) this.navCtrl.setRoot('AboutPage');
    this.vaderPissedLevel++;    
  }

  getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;  
  }

  VaderAnnoys(){    
    switch(this.gameResult.winner){
      case "computer":{        
        this.userWonLastRound=false;
        if(this.vaderWonLastRound){
          this.sound.Play(this.whenVaderWinsInSequence[this.getRandomIntInclusive(0,this.whenVaderWinsInSequence.length-1)])
          .catch(e=>console.log(e));
        } else {
          this.sound.Play(this.whenVaderWins[this.getRandomIntInclusive(0,this.whenVaderWins.length-1)])
          .catch(e=>console.log(e));
          this.vaderWonLastRound=true;
        }        
        break;
      }
      case "user":{        
        this.vaderWonLastRound=false;
        if(this.userWonLastRound){
          this.sound.Play(this.whenVaderLoosesInSequence[this.getRandomIntInclusive(0,this.whenVaderLoosesInSequence.length-1)])
          .catch(e=>console.log(e));
        } else {
          this.sound.Play(this.whenVaderLooses[this.getRandomIntInclusive(0,this.whenVaderLooses.length-1)])
          .catch(e=>console.log(e));
          this.userWonLastRound=true;
        }         
        break;
      }
      case "tie":{
        this.vaderWonLastRound=false;
        this.userWonLastRound=false;
        break;
      }
    }
  }

  TakePicture(){
    this.device.TakePicture().then(data=>{
      console.log('Img URI:',data);
      this.userImg=data;
    })    
  }  

}
