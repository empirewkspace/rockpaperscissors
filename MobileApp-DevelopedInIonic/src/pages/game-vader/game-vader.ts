import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SoundMatrixProvider } from '../../providers/sound-matrix/sound-matrix';

@IonicPage()
@Component({
  selector: 'page-game-vader',
  templateUrl: 'game-vader.html',
})
export class GameVaderPage {

  private spock: boolean = false;

  private name: string;
  private disablePlayButton: boolean = true;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public sound: SoundMatrixProvider
  ) {

  }

  async ionViewDidEnter() {
    try{
    await this.sound.Play('padawan')
    this.disablePlayButton = false;
    } catch(e){
      console.log(e);
    }

  }

  async goto(page) {
    try {
      await this.sound.Play('lastMistake', 'lightSaber');      
    } catch (e) {
      console.log(e);
    }
    let nElements = 3;
    if (this.spock == true) nElements = 5;
    if(!this.name) this.name="Padawan";
    this.navCtrl.setRoot(page, { "userName": this.name, "nElements": nElements });        
  }


}
