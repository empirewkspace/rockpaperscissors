import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnlineTournamentPage } from './online-tournament';

@NgModule({
  declarations: [
    OnlineTournamentPage,
  ],
  imports: [
    IonicPageModule.forChild(OnlineTournamentPage),
  ],
})
export class OnlineTournamentPageModule {}
