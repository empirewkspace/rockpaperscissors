import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { DataProvider, User } from '../../providers/data/data';
import { SoundMatrixProvider } from '../../providers/sound-matrix/sound-matrix';
import { SceneProvider, Movie } from '../../providers/scene/scene';

@IonicPage()
@Component({
  selector: 'page-online-tournament',
  templateUrl: 'online-tournament.html',
})
export class OnlineTournamentPage {

  private email: string = "";
  private password: string = "";
  private name: string = "";
  private register = 'Register';
  private showName = false;

  constructor(public navCtrl: NavController,
    public api: ApiProvider,
    public data: DataProvider,
    public soundCtrl: SoundMatrixProvider,
    public sceneProvider: SceneProvider,
    public modalCtrl: ModalController
  ) {
  }

   ionViewWillEnter() {
    if (this.data.token) this.navCtrl.setRoot('LobbyPage');    
  }

  ionViewDidEnter(){
    this.soundCtrl.Play('themeOnlineTournament');
  }

  async Movie(i:number):Promise<boolean>{
    let movie = this.sceneProvider.movieMatrix.find(p=>p.movie==i);
    return await this.PlayMovie(movie);
  }  

  PlayMovie (movie:Movie): Promise<boolean>{
    return new Promise((resolve,reject)=>{
      let mymodal = this.modalCtrl.create('VaderReactionsPage',{'Movie':movie});
      mymodal.present();
      mymodal.onDidDismiss(()=>resolve(true));
    });
  }  

  async ionViewWillLeave() {
    await this.soundCtrl.Stop('themeOnlineTournament');
  }

  Login() {
    this.api.authenticateUser(this.email, this.password)
      .then(res => {
        this.data.token = res;
        console.log('Token received', this.data.token);
        this.api.logUserInTheLobby()
          .then(async () => {
            console.log(this.data.loggedUser);
            try{        
            await this.soundCtrl.Stop('themeOnlineTournament');    
            await this.Movie(1);
            } catch (e) {console.log(e)}
            this.navCtrl.setRoot('LobbyPage');
          })
          .catch(e => alert(e.error));
      })
      .catch(e => alert(e.error));
  }

  Register() {
    this.register = 'Sign Up';
    if (this.showName) {
      this.api.createUser(this.name, this.email, this.password).then(
        () => { this.showName = false; this.register = "Register"; alert('User created!'); }
      ).catch(e => { alert(e.error); });
    }
    this.showName = true;
  }


}
