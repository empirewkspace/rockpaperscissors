import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-choice-user',
  templateUrl: 'choice-user.html',
})
export class ChoiceUserPage {

  private options:String[] = [];

  constructor(public view: ViewController, public params: NavParams) {
  }

  ionViewWillEnter(){

    this.options = this.params.get('options');

  }

  Dismiss(i:number) {    
    this.view.dismiss(i);
  }


}
