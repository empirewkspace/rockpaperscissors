import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { SoundMatrixProvider } from '../../providers/sound-matrix/sound-matrix';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private name:string;

  constructor(
    public navCtrl: NavController,
    public sound: SoundMatrixProvider
  ) {

  }

  ionViewDidEnter(){
    this.sound.Play('whatsWrongMacFlyChicken').catch(e=>console.log(e));
  }
  

  goto(page){
    this.navCtrl.setRoot(page,{"userName":this.name});
  }

}
