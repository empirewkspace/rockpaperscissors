import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { SceneProvider, Scene, Movie } from '../../providers/scene/scene';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  public sceneIndex;
  public scenes:Scene[] = [];
  public movies:Movie[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public sceneProvider: SceneProvider, public modalCtrl: ModalController
  ) {
    this.scenes = this.sceneProvider.sceneMatrix;
    this.movies = this.sceneProvider.movieMatrix;
  }

  
  Play(scene: Scene){
    let mymodal = this.modalCtrl.create('VaderReactionsPage',{'Scene':scene});
    mymodal.present();
  }

  PlayMovie (movie:Movie): Promise<boolean>{
    return new Promise((resolve,reject)=>{
      let mymodal = this.modalCtrl.create('VaderReactionsPage',{'Movie':movie});
      mymodal.present();
      mymodal.onDidDismiss(()=>resolve(true));
    });
  }

  async Movie(i:number){
    let movie = this.movies.find(p=>p.movie==i);
    await this.PlayMovie(movie);
  }  

}
