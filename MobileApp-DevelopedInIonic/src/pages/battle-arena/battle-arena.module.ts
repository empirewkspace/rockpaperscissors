import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BattleArenaPage } from './battle-arena';

@NgModule({
  declarations: [
    BattleArenaPage,
  ],
  imports: [
    IonicPageModule.forChild(BattleArenaPage),
  ],
})
export class BattleArenaPageModule {}
