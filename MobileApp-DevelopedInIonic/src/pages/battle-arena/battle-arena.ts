import { Component, Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { GameRoom, ApiProvider, GameOutcome } from '../../providers/api/api';
import { DataProvider, User } from '../../providers/data/data';
import { GameLogicProvider } from '../../providers/game-logic/game-logic';
import { SoundMatrixProvider } from '../../providers/sound-matrix/sound-matrix';
import { Scene, SceneProvider, Movie } from '../../providers/scene/scene';

//IMPLEMENTING STATE DESIGN PATTERN
//STATES: noOneHavePlayed, aSingleUserHasPlayed, bothUsersHavePlayed, whoWon

interface IGameRoomState {

  context: BattleArenaPage;
  Process();
  Play(userChoice: number): Promise<boolean>;

}

class AllUsersHavePlayed implements IGameRoomState {

  constructor(public context: BattleArenaPage) { }

  async Process() {

    //only roomOwnerFinishes Calculations and therefore reached this end
    await this.context.WhoIsAChicken();
    try {
      let roomOwnerChoice = this.context.gameRoom.roomOwnerChoice;
      let challengerChoice = this.context.gameRoom.challengerChoice;
      this.context.gameRoom = await this.context.api.CalculateResults(
        this.context.gameRoom._id, roomOwnerChoice, challengerChoice);
      this.context.WhoWon();
      this.context.state = new NoOneHavePlayed(this.context);
    } catch (e) { console.log(e) }
  }

  async Play(userChoice: number): Promise<boolean> {
    return Promise.resolve(true);
  }
}

class OneUserHavePlayed implements IGameRoomState {

  constructor(
    public context: BattleArenaPage
  ) { }

  async Process() {
    await this.context.WhoIsAChicken();
    if (
      this.context.gameRoom.roomOwner == this.context.data.loggedUser._id &&
      this.context.gameRoom.roomOwnerChoice !== undefined &&
      this.context.gameRoom.challengerChoice !== undefined) {
      this.context.state = new AllUsersHavePlayed(this.context);
    } else {
      if (
        (this.context.challenger == this.context.data.loggedUser) &&
        (this.context.gameRoom.game == this.context.game + 1)) {
        //means that you're not in the same round
        //you are the challenger and have to wait until roomOwner finishes calculations
        //when he finishes, all choices are eliminated from the gameRoom and game counter is increased
        this.context.game = this.context.gameRoom.game;
        this.context.WhoWon(); //just get the last result and check who won
        this.context.state = new NoOneHavePlayed(this.context);
      }
    }

  }
  async Play(userChoice: number): Promise<boolean> {
    try {
      await this.context.api.WriteUserChoiceOnGameRoom(this.context.gameRoom._id, userChoice);
      this.Process();
    } catch (e) { console.log(e); this.context.playBtnDisable = false; }
    return Promise.resolve(true);
  }

}


class NoOneHavePlayed implements IGameRoomState {

  constructor(public context: BattleArenaPage) { }

  async Process() {
    await this.context.WhoIsAChicken();
    if ((this.context.gameRoom.roomOwnerChoice !== undefined) || (this.context.gameRoom.challengerChoice !== undefined)) {
      this.context.state = new OneUserHavePlayed(this.context);
    }
  }
  async Play(userChoice: number): Promise<boolean> {
    try {
      await this.context.api.WriteUserChoiceOnGameRoom(this.context.gameRoom._id, userChoice);
      this.Process();
    } catch (e) { console.log(e); this.context.playBtnDisable = false; }
    return Promise.resolve(true);
  }
}


class RoomOwnerIsPlayingAlone implements IGameRoomState {
  constructor(public context: BattleArenaPage, public whoFled?: string) {
    this.context.roomOwnerScore = 0;
    this.context.challenger = null;
    this.context.api.ClearGameRoomResults(this.context.gameRoom._id);
  }
  async Process() {

    if (this.whoFled == 'challenger') {
      this.whoFled = null;
      await this.context.soundCtrl.Stop('themeFightArena');
      await this.context.Movie(4);
      await this.context.soundCtrl.PlayMusicOnLoop('themeFightArena', 0.4);
    }

    try {
      this.context.gameRoom = await this.context.api.GetGameRoom(this.context.gameRoom._id);

      if (this.context.gameRoom.challenger !== undefined) {
        this.context.gameRoom = await this.context.api.ClearGameRoomResults(this.context.gameRoom._id);
        this.context.gameResults = [];
        this.context.roomOwnerScore = 0;
        this.context.challengerScore = 0;
        this.context.playBtnDisable = false;
        this.context.challenger = await this.context.api.getUser(this.context.gameRoom.challenger);
        alert('A Challenger have entered the room');
        this.context.state = new NoOneHavePlayed(this.context);
      }
    } catch (e) { console.log(e); }
  }

  async Play(userChoice: number): Promise<boolean> {
    try {
      await this.context.api.WriteUserChoiceOnGameRoom(this.context.gameRoom._id, userChoice);
      this.context.gameRoom = await this.context.api.CalculateResults(this.context.gameRoom._id, userChoice);
      this.context.WhoWon();
    } catch (e) { console.log(e); this.context.playBtnDisable = false; }
    return Promise.resolve(true);
  }

}


@IonicPage()
@Component({
  selector: 'page-battle-arena',
  templateUrl: 'battle-arena.html',
})
export class BattleArenaPage { //BattleArenaPlage is the CONTEXT 

  public interval: any;
  public state: IGameRoomState;
  public gameRoom: GameRoom;
  public roomOwner: User;
  public challenger: User;
  public roomOwnerScore = 0;
  public challengerScore = 0;
  public gameResults: Array<GameOutcome> = [];
  public playBtnDisable = false;
  public game: number;

  public vaderHappyReactions = [36, 37, 38, 39, 40, 41, 42, 43];
  public vaderVeryHappyReactions = [32, 33, 34, 35];
  public vaderDispleasedReactions = [23, 24, 25, 26, 27, 28, 29, 30, 31, 17, 18, 19, 20, 21, 22, 1, 2, 3, 4, 5];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public api: ApiProvider,
    public data: DataProvider,
    public gameLogic: GameLogicProvider,
    public platform: Platform,
    public soundCtrl: SoundMatrixProvider,
    public sceneProvider: SceneProvider
  ) {
    /* let backBtn = platform.registerBackButtonAction(() => {
      backBtn(); //unregister
      this.ChickenOut();
    }); */

  }

  async ionViewWillEnter() {
    try {
      this.soundCtrl.PlayMusicOnLoop('themeFightArena', 0.4); //second parameter is volume
      this.gameRoom = this.navParams.get('gameRoom');
      if (this.gameRoom.roomOwner == this.data.loggedUser._id) {
        this.roomOwner = this.data.loggedUser;
        this.state = new RoomOwnerIsPlayingAlone(this);
      } else {
        this.roomOwner = await this.api.getUser(this.gameRoom.roomOwner);
        this.game = this.gameRoom.game;
        this.challenger = this.data.loggedUser;
        this.state = new NoOneHavePlayed(this);
      }

      this.interval = setInterval(async () => {

        await this.state.Process();
      }, 1000);

    } catch (e) { console.log(e) }

  }

  ionViewWillLeave() {
    clearInterval(this.interval);
    this.soundCtrl.Stop('themeFightArena');
  }

  Title(wins: number, losses: number) {
    return this.gameLogic.Title(wins, losses);
  }

  async Movie(i: number): Promise<boolean> {
    let movie = this.sceneProvider.movieMatrix.find(p => p.movie == i);
    return await this.PlayMovie(movie);
  }

  PlayMovie(movie: Movie): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let mymodal = this.modalCtrl.create('VaderReactionsPage', { 'Movie': movie });
      mymodal.present();
      mymodal.onDidDismiss(() => resolve(true));
    });
  }

  PlayScene(scene: Scene, title: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let mymodal = this.modalCtrl.create('VaderReactionsPage', { 'Scene': scene, 'Title': title });
      mymodal.present();
      mymodal.onDidDismiss(() => resolve(true));
    });
  }

  async ChickenOut() {

    if (this.data.loggedUser._id == this.gameRoom.roomOwner) {
      try {
        await this.api.DeleteGameRoom();        
      } catch (e) { console.log(e) }
      if (this.challenger) {
        this.navCtrl.setRoot('LobbyPage', { 'roomOwnerIsAChicken': this.gameRoom.roomOwner })
      } else { this.navCtrl.setRoot('LobbyPage');   }
    
    } else {
      try {
        await this.api.ChallengerLeaveRoom(this.gameRoom._id);
      } catch (e) { console.log(e) }
      this.navCtrl.setRoot('LobbyPage', { 'challengerIsAChicken': this.gameRoom.challenger });
    }

  }

  async WhoIsAChicken() {//for 2 players in the room
    try {
      this.gameRoom = await this.api.GetGameRoom(this.gameRoom._id);
      if (this.gameRoom.challenger == undefined) {
        this.roomOwnerScore = 0;
        this.challengerScore = 0;
        this.challenger = null;
        this.playBtnDisable = false;
        this.state = new RoomOwnerIsPlayingAlone(this, 'challenger');//challenger fled
      }
    } catch (e) {
      //no gameRoom? RoomOwner FLED like a coward       
      clearInterval(this.interval);
      this.navCtrl.setRoot('LobbyPage', { 'roomOwnerIsAChicken': this.gameRoom.roomOwner });
    }
  }

  Play() {
    try {
      let objs = this.gameLogic.objects;
      this.playBtnDisable = true;
      let myModal = this.modalCtrl.create('ChoiceUserPage', { 'options': objs });
      myModal.present();
      myModal.onDidDismiss(async (userChoice) => {
        await this.state.Play(userChoice);
      }
      );
    } catch (e) { console.log(e) }

  }

  async WhoWon() {
    try {
      this.gameRoom = await this.api.GetGameRoom(this.gameRoom._id);
      this.playBtnDisable = false;
      this.gameResults = this.gameRoom.results.reverse();
      let results = this.gameResults[0];
      if (results.winner == this.gameRoom.roomOwner) {
        this.roomOwnerScore++;
        let rand = Math.floor(Math.random() * 8);
        let scene = this.sceneProvider.FetchScene(this.vaderHappyReactions[rand]);
        await this.PlayScene(scene, "You have won");
        if (this.roomOwnerScore >= 20) {
          await this.soundCtrl.Stop('themeFightArena');
          await this.Movie(6);
          this.LeaveRoomBecauseRoundIsComplete();
        }

      }
      if (results.winner == this.gameRoom.challenger || results.winner == 'computer') {
        let sceneLooser = this.sceneProvider.FetchScene(this.vaderDispleasedReactions[this.challengerScore]);
        this.challengerScore++;
        await this.PlayScene(sceneLooser, "You have lost");
        if (this.challengerScore >= 20) {
          let rand = Math.floor(Math.random() * 2 + 2);
          await this.soundCtrl.Stop('themeFightArena');
          await this.Movie(rand);
          this.LeaveRoomBecauseRoundIsComplete();
        }
      }

    } catch (e) { console.log(e) }
  }

  async LeaveRoomBecauseRoundIsComplete() {
    if (this.data.loggedUser._id == this.gameRoom.roomOwner) {
      try {
        await this.api.DeleteGameRoom();
      } catch (e) { console.log(e) }
      this.navCtrl.setRoot('LobbyPage');
    } else {
      try {
        await this.api.ChallengerLeaveRoom(this.gameRoom._id);
      } catch (e) { console.log(e) }
      this.navCtrl.setRoot('LobbyPage');
    }
  }

}
