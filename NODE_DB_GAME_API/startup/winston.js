//Error module LOG for logging errors:
//in a file: npm i winston
//in mongo: npm i winston-mongodb@3.0.0
//for async errors: npm i express-async-errors
const winston = require('winston');
require('winston-mongodb');
require('express-async-errors');

module.exports = function () {

    var uri=`mongodb+srv://${process.env.usuario}:${process.env.senha}@cluster0-1l9gv.mongodb.net/rockPaperScissorsGame?retryWrites=true`;
    
    winston.add(winston.transports.File, {
        filename: 'logfile.log'
    });
    /* winston.add(winston.transports.MongoDB, {
        db: uri
    }); */

    winston.handleExceptions(
        new winston.transports.Console({colorize: true, prettyPrint:true}),
        new winston.transports.File({
            filename: 'uncaughtExceptions.log'
        })
    );

    process.on('unhandledRejection', (ex) => {
        //for errors on promises...
        /*console.log('WE GOT AN UNHANDLED REJECTION');
        winston.error(ex.message,ex);
        process.exit(1);*/
        throw ex; //vai jogar a execução para winston.handleExceptions
    });

    //testing unhandled Rejections
    /* const p = Promise.reject(new Error('Something failed miserably!'));
    p.then(()=>console.log('Done'));  */
    //testando unhandled Exceptions
    //throw new Error('oops2');
}