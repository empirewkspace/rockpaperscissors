//imports
const express = require('express');
const app = express();

require('./startup/winston')(); //for logging unhandled errors
require('./startup/config')(); //config module
require('./startup/routes')(app); //rotas
require('./startup/db')(); //database 
require('./startup/validation')(); //validation logic

//http server
const winston = require('winston');
const port = process.env.PORT || 3001;
app.listen(port, () => winston.info(`Listening on ${port}...`));



