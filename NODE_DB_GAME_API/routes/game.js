//external modules
const _ = require('lodash');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

//****/
const auth = require('../middlewares/auth');
const asyncMiddleware = require('../middlewares/async');
const {User} = require('../models/user');
const {GameRoom,CreateGameRoom, validate} = require('../models/gameRoom');
const {GameLogic} = require('../models/game');

//*Routes


router.get('/playersOnline/:pageNumber/:pageSize', asyncMiddleware(async (req, res) => {
    let {pageNumber, pageSize} = req.params;
    pageNumber = parseInt(pageNumber);
    pageSize = parseInt(pageSize);
    let users = await User.find({online: true}).skip((pageNumber - 1) * pageSize)
        .limit(pageSize).sort({email: 1}).select('-password');
    res.status(200).send(users);
}));

router.post('/createGameRoom', auth, asyncMiddleware(async (req, res) => {
    //body must have nElements (0-4 if Spock is active)
    // isPrivate and password property
    let gameRoom = await CreateGameRoom(req.user._id, req.body.nElements, req.body.isPrivate, req.body.password);
    if (await GameRoom.findOne({roomOwner: req.user._id})) {
        res.status(400).send('user already have a room');
    } else {
        await gameRoom.save();
        gameRoom = await GameRoom.findOne({roomOwner: req.user._id})
        res.status(200).send(gameRoom);
    }
}));

router.post('/challengerEnterRoom', auth, asyncMiddleware(async (req, res) => {
    let gameRoom = await GameRoom.findOneAndUpdate({_id: req.body._id}, {
        challenger: req.user._id});
    if (!gameRoom) res.status(400).send('Room no longer available');
    await gameRoom.save();
    gameRoom = await GameRoom.findOne({_id: req.body._id});
    res.status(200).send(gameRoom);
}));

router.post('/challengerLeaveRoom', asyncMiddleware(async (req, res) => {
    let gameRoom = await GameRoom.findOneAndUpdate({_id: req.body._id}, {
        $unset: {challenger: 1,challengerChoice: 1}});
    if (!gameRoom) res.status(400).send('Room no longer available');
    await gameRoom.save();
    gameRoom = await GameRoom.findOne({_id: req.body._id});
    res.status(200).send(gameRoom);
}));

router.post('/checkIfChallengerIsInTheRoom', asyncMiddleware(async (req, res) => {
    let gameRoom = await GameRoom.findOne({_id: req.body._id});    
    if(!gameRoom) res.status(400).send('GameRoom does not exist!');
    if(gameRoom.challenger!==undefined){
        res.status(200).send('true');
    }else {
        res.status(400).send('false');
    }    
}));

router.post('/checkIfRoomIsPrivate', asyncMiddleware(async (req, res) => {
    let gameRoom = await GameRoom.findOne({_id: req.body._id});    
    if(!gameRoom) res.status(400).send('GameRoom does not exist!');
    if(gameRoom.isPrivate==true){
        res.status(200).send('true');
    }else {
        res.status(400).send('false');
    }    
}));

router.get('/availableGameRooms', asyncMiddleware(async (req, res) => {
    let gameRooms = await GameRoom.find({});    
    res.status(200).send(gameRooms);
}));

router.delete('/deleteGameRoom', auth, asyncMiddleware(async (req, res) => {
    let gameRoom = await GameRoom.findOne({roomOwner: req.user._id});
    if (!gameRoom) res.status(400).send("You don't own any room!");
    await GameRoom.findOneAndRemove({roomOwner: req.user._id});
    let gameRooms = await GameRoom.find({});
    res.status(200).send(gameRooms);
}));

router.post('/deleteGameRoomWithPost', auth, asyncMiddleware(async (req, res) => {
    let gameRoom = await GameRoom.findOne({roomOwner: req.user._id});
    if (!gameRoom) res.status(400).send("You don't own any room!");
    await GameRoom.findOneAndRemove({roomOwner: req.user._id});    
    res.sendStatus(200);
}));

router.post('/writeUserChoiceOnGameRoom', auth, asyncMiddleware(async (req, res) => {
    //body must have: _id of the gameRoom, userChoice (0-4 if Spock is active)    
    let gameRoom = await GameRoom.findOne({_id: req.body._id}); //gameRoom ID
    if (!gameRoom) res.status(400).send("GameRoom not found!");
    let roomOwnerId = gameRoom.roomOwner;
    let challengerId = gameRoom.challenger;
    if (req.user._id == roomOwnerId) 
    {
        await GameRoom.findOneAndUpdate({_id:req.body._id},{roomOwnerChoice:req.body.userChoice});
    }
    if (req.user._id == challengerId) {
        await GameRoom.findOneAndUpdate({_id:req.body._id},{challengerChoice:req.body.userChoice});
    }
    
    gameRoom = await GameRoom.findOne({_id:req.body._id});
    if (!gameRoom) res.status(400).send("GameRoom not found!");
    res.status(200).send(gameRoom);
}));

router.post('/getGameRoom', asyncMiddleware(async (req, res) => {
    let gameRoom = await GameRoom.findOne({_id: req.body._id});
    if (!gameRoom) res.status(400).send("GameRoom not found!");
    res.status(200).send(gameRoom);
}));

router.post('/getMyGameRoom', auth, asyncMiddleware(async (req, res) => {
    let gameRoom = await GameRoom.findOne({roomOwner: req.user._id});
    if (!gameRoom) res.status(400).send("GameRoom not found!");
    res.status(200).send(gameRoom);
}));

router.post('/clearGameRoomResults', asyncMiddleware(async (req, res) => {
    let gameRoom = await GameRoom.findOneAndUpdate({_id: req.body._id},{results:[],
    $unset:{userChoice:1,challengerChoice:1}});
    if (!gameRoom) res.status(400).send("GameRoom not found!");    
    gameRoom = await GameRoom.findOne({_id: req.body._id});
    res.status(200).send(gameRoom);
}));

router.post('/lockGameRoom', auth, asyncMiddleware(async (req,res)=>{
    var gameRoom = await GameRoom.findOne({_id: req.body._id});    
    if (!gameRoom) res.status(400).send("GameRoom not found!");
    if(gameRoom.whoLockedRoom==undefined){
        await GameRoom.findOneAndUpdate({_id:req.body._id},{whoLockedRoom:req.user._id});
        res.sendStatus(200);
    } else {
        res.status(400).send('This Room is already Locked by someone');        
    }           
}));

router.post('/unlockGameRoom', auth, asyncMiddleware(async (req,res)=>{
    var gameRoom = await GameRoom.findOne({_id: req.body._id});    
    if (!gameRoom) res.status(400).send("GameRoom not found!");
    if(gameRoom.whoLockedRoom==req.user._id){
        await GameRoom.findOneAndUpdate({_id:req.body._id},{$unset:{whoLockedRoom:1}});
        res.sendStatus(200);
    } else {
        res.status(400).send('You are not allowed to Unlock this room');        
    }    
}));

router.post('/calculateResults', auth, asyncMiddleware(async (req, res) => {
    
    var gameRoom = await GameRoom.findOne({_id: req.body._id});    
    if (!gameRoom) res.status(400).send("GameRoom not found!");

    /* if(gameRoom.whoLockedRoom==req.user._id){        */
        
        var nElements = gameRoom.nElements;
        var roomOwnerChoice = req.body.roomOwnerChoice;
        var roomOwnerId = gameRoom.roomOwner;
        var challengerChoice = req.body.challengerChoice;
        var challengerId = gameRoom.challenger;        

        if (roomOwnerChoice!==undefined&&challengerChoice!==undefined) {
            console.log('Both Users have Chosen a Hand!!!');
            var gameLogic = new GameLogic(roomOwnerId, challengerId);
            var gameOutcome = gameLogic.Play(nElements, roomOwnerChoice, challengerChoice);

            await GameRoom.findOneAndUpdate({_id: req.body._id}, {
            $push: {results: gameOutcome}, $inc:{game:1},
             $unset: {roomOwnerChoice:1, challengerChoice:1}
            });

            if(gameOutcome.winner==roomOwnerId) {
                await User.findOneAndUpdate({_id:roomOwnerId},{$inc:{wins:1}});
                await User.findOneAndUpdate({_id:challengerId},{$inc:{losses:1}});
            }
            if(gameOutcome.winner==challengerId) {
                await User.findOneAndUpdate({_id:challengerId},{$inc:{wins:1}});
                await User.findOneAndUpdate({_id:roomOwnerId},{$inc:{losses:1}});
        }

        gameRoom = await GameRoom.findOne({_id: req.body._id});    
        if (!gameRoom) res.status(400).send("GameRoom not found in the End!");
        res.status(200).send(gameRoom);
        } 
        else {
            //perhaps user is playing alone?
            if (gameRoom.challenger==undefined) {            
                var gameLogic = new GameLogic(roomOwnerId, "computer");
                var gameOutcome = gameLogic.Play(nElements, roomOwnerChoice);
                await GameRoom.findOneAndUpdate({_id: req.body._id},
                    {
                        $push: {results: gameOutcome},
                        $unset: {roomOwnerChoice:1}        
                    });

                gameRoom = await GameRoom.findOne({'_id':req.body._id});
                res.status(200).send(gameRoom);
            } 
            else {
                res.status(400).send('{"message":"calculated results was called, without a challengerChoice?"}');
            }
        }
        
   /*  } else {
        res.status(400).send('{"message":"Someone Locked the Room...and only he can unlock"}');
    } */


}));

module.exports = router;