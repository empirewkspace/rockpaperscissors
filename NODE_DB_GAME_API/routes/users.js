const jwt = require('jsonwebtoken');
const config = require('config');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const {
    User,
    validate
} = require('../models/user');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const asyncMiddleware = require('../middlewares/async');
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
        user: "funnyrockpaperscissors@gmail.com",
        pass: "funnygame"
    },
    tls: {
        rejectUnauthorized: false
    }
});

router.get('/me', auth, asyncMiddleware(async (req, res) => {
    const user = await User.findOne({
        _id: req.user._id
    }).select('-password');
    res.send(user);
}));

router.post('/', asyncMiddleware(async (req, res) => {

    const {
        error
    } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let user = await User.findOne({
        email: req.body.email
    });
    if (user) return res.status(400).send('User already registered.');

    user = new User(_.pick(req.body, ['name', 'email', 'password']));

    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(user.password, salt);
    await user.save();

    const token = user.generateAuthToken();
    res.header('x-auth-token', token).send(_.pick(user, ['_id', 'name', 'email']));
}));

router.delete('/', auth, asyncMiddleware(async (req, res) => {
    const user = await User.findOne({
        _id: req.user._id
    });
    await User.findOneAndRemove({
        _id: req.user._id
    });
    res.status(200).send(`User ${user.email} DELETED`);
}));

router.post('/readUserStats', asyncMiddleware(async (req, res) => {
    let user = await User.findOne({
        _id: req.body._id
    }).select({
        name: 1,
        wins: 1,
        losses: 1
    });
    res.status(200).send(user);
}));

router.post('/login', auth, asyncMiddleware(async (req, res) => {
    await User.findByIdAndUpdate(req.user._id, {
        online: true
    });
    let user = await User.findById(req.user._id).select('-password');
    if (!user) return res.status(400).send('user ID not found');
    res.status(200).send(user);
}));

router.post('/logout', auth, asyncMiddleware(async (req, res) => {
    await User.findByIdAndUpdate(req.user._id, {
        online: false
    });
    let user = await User.findById(req.user._id).select('-password');
    if (!user) return res.status(400).send('user ID not found');
    res.status(200).send(user);
}));

router.get('/forgotPassword/:email', asyncMiddleware(async (req, res) => {

    /*   let {email} = req.params;
      console.log('email', email);

      let mailOptions = {
          from: 'chaves.camilo@gmail.com',
          to: email,
          subject: 'Sending Email using Node.js',
          text: 'That was easy!'
        };
        
      transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
            res.status(400).send(error);
          } else {
            console.log('Email sent: ' + info.response);
            res.status(200).send(info.response);
          } */
}));




module.exports = router;