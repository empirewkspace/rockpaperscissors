module.exports = function (req,res,next){
    //auth middleware sets req.user
    //401 Unauthorized
    //403 Forbidden 
    //500 server failure
   
    if(!req.user.isAdmin) return res.status(403).send('Access denied.');
    next();
}