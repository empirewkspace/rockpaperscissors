function GameLogicProvider(roomOwnerId, challengerId) {

    this.possibleOutcomes = {
        "scissors paper": "scissors cuts paper",
        "paper rock": "paper covers rock",
        "rock scissors": "rock crushes scissors",
        "spock rock": "spock vaporizes rock",
        "spock scissors": "spock smashes scissors",
        "paper spock": "paper disproves spock",
        "scissors lizard": "scissors decapitates lizard",
        "lizard paper": "lizard eats paper",
        "rock lizard": "rock crushes lizard",
        "lizard spock": "lizard poisons spock"
    };

    this.objects = ["scissors", "paper", "rock", "spock", "lizard"];

    this.roomOwnerId = roomOwnerId;
    this.challengerId = challengerId;
}

GameLogicProvider.prototype.rollYourDice = function (n) {
    let result = Math.floor(Math.random() * n);
    if (result == this.objects.length) result--;
    return result;
}

GameLogicProvider.prototype.whoWins = function (user1, user2) {
    outcome = this.objects[user1] + " " + this.objects[user2];
    if (user1 == user2) {
        return {
            winner: "tie",
            outcome: "tie"
        };
    }
    if (this.possibleOutcomes[outcome]) {
        return {
            winner: this.roomOwnerId,
            outcome: this.possibleOutcomes[outcome]
        };
    } else {
        outcome = this.objects[user2] + " " + this.objects[user1];
        return {
            winner: this.challengerId,
            outcome: this.possibleOutcomes[outcome]
        };
    }
}

GameLogicProvider.prototype.Play = function (nElements, user1, user2) {
    if (user2 == undefined || user2 == null) {
        //then user1 is playing against computer
        user2 = this.rollYourDice(nElements);
    }
    const result = this.whoWins(user1, user2);
    
    return {
        user1Tool: this.objects[user1],
        user2Tool: this.objects[user2],
        outcome: result.outcome,
        winner: result.winner
    };
}


module.exports.GameLogic = GameLogicProvider;