const Joi = require('joi');
const mongoose = require('mongoose');
const User = require('../models/user');

const gameOutcomes = new mongoose.Schema({
    user1Tool: String,
    user2Tool: String,
    outcome: String,
    winner: String
})

const gameRoomSchema = new mongoose.Schema({
    room: {
        type: Number,
        default: 0,
        required: true
    },
    game: {
        type: Number,
        default: 1,
        required: true
    },
    roomOwner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        unique: true
    },
    challenger: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    date: {
        type: Date,
        default: Date.now()
    },
    isPrivate: {
        type: Boolean,
        default: false,
        required: true
    },
    whoLockedRoom: String,
    password: String,
    roomOwnerChoice: Number,
    challengerChoice: Number,
    nElements: Number,
    results: [gameOutcomes]
}, {
    versionKey: false
});

const GameRoom = mongoose.model('GameRoom', gameRoomSchema);

function validateGameRoom(gameRoom) {
    const schema = {
        room: Joi.number().required(),
        game: Joi.number().required(),
        roomOwner: Joi.object().required(),
        nElements: Joi.number().required(),
        isPrivate: Joi.boolean().required()
    };
    return Joi.validate(gameRoom, schema);
}

async function CreateGameRoom(userId, nElements, isPrivate, password) {
    let roomCount = await GameRoom.collection.countDocuments();
    if (!roomCount) roomCount = 0;
    roomCount++;
    if (isPrivate == true) {
        return new GameRoom({
            room: roomCount,
            game: 0,
            roomOwner: userId,
            nElements: nElements,
            isPrivate: isPrivate,
            password: password
        });
    }

    return new GameRoom({
        room: roomCount,
        game: 0,
        roomOwner: userId,
        nElements: nElements
    });
}

exports.GameRoom = GameRoom;
exports.validate = validateGameRoom;
exports.CreateGameRoom = CreateGameRoom;